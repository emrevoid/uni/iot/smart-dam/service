var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    // "backend" code for the dashboard
    console.log('DashTime: ', Date.now());
    next();
}, (req, res) => {
    res.render('dashboard', { title: 'Dashboard' });
});

module.exports = router;