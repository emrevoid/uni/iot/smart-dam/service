/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

var express = require('express');
var path = require('path');
var dashboardRouter = require('./routes/dashboard');
var pwaRouter = require('./routes/pwa');
var levelDB = require('./config/db');

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use('/', dashboardRouter);
app.use('/pwa', pwaRouter);

var server = app.listen(3000, function() {
    console.log('Node server is running..');
})


function saveToDB(ts, l) {
    const smartDamDB = new levelDB({
        timestamp: ts,
        level: l
    })

    smartDamDB.save().then(d => {
        console.log('entry added');
    }).catch(error => {
        console.log('error adding entry: ' + error);
    })
}

// saveToDB(Date.now(), 0.8);
// saveToDB(Date.now(), 0.9);
// saveToDB(Date.now(), 0.3);